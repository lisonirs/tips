package application;

import java.io.*;
import java.io.FileWriter;
import java.net.URL;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

//ObjetInputStream = stocker objet java : java peut aller le lire

public class ControllerTips implements Initializable{
	
	@FXML
	private Button calculate;
	
	@FXML
	private TextField bill;
	
	@FXML
	private TextField tip;
	
	@FXML
	private TextField nbPeople;
	
	@FXML
	private TextField tipTotal;
	
	@FXML
	private TextField totalPP;
	
	@FXML
	private Label error;
	
	@FXML
	private DatePicker date;
	
	@FXML
	private LocalDate dateValue;
	
	@FXML
	private int billText;
	
	@FXML
	private int tipText;
	
	@FXML
	private int nbPeopleText;
	
	
	public void Calculate(){
		error.setText("");
		try {
			float billText = Integer.parseInt(bill.getText());
			
		}catch (NumberFormatException e){
			error.setText("Le champ saisi dans Bill doit �tre un nombre");
		}
		
		try {
			float tipText = Integer.parseInt(tip.getText());
			
		}catch (NumberFormatException e){
			error.setText("Le champ saisi dans Tip % doit �tre un nombre");
		}
		
		try {
			float nbPeopleText = Integer.parseInt(nbPeople.getText());
			
		}catch (NumberFormatException e){
			error.setText("Le champ saisi dans Nb People doit �tre un nombre");
		}
		
		
		dateValue = date.getValue();
		//System.out.println(dateValue);
		if(dateValue == null) {
			error.setText("Le champ saisi dans Date doit �tre de type jj/mm/yyyy");
			System.out.println(dateValue);
		}
			
			float tipTotalF = ((tipText/100)*billText)/nbPeopleText;
			float totalPPF = (billText/nbPeopleText)+tipTotalF;
			
			tipTotal.setText(Float.toString(tipTotalF));
			totalPP.setText(Float.toString(totalPPF));
			
			try {
			FileWriter writer = new FileWriter("Tips.txt", true);
			writer.write(dateValue + " ;" + billText + " ;" + tipText + " ;" + nbPeopleText);
			writer.write("\n");
			 
			writer.close();
			
			
			}catch(FileNotFoundException e){
				error.setText("Fichier non trouv�");
				
			}catch(IOException e){
				error.setText("Erreur de fichier");
			}
			
	}

		public static List<String[]> readFile(String urlFichier) {
	        BufferedReader lecteurAvecBuffer = null;
	        List<String[]> listeDocument = new ArrayList<String[]>();
	        try {
	            lecteurAvecBuffer =
	                new BufferedReader(
	                    new InputStreamReader(new FileInputStream(urlFichier), "UTF8"));
	            String ligne = "";
	            int nbLigne = 1;
	            while ((ligne = lecteurAvecBuffer.readLine()) != null) {
	                if (nbLigne > 0) {
	                    String[] tab = ligne.split(";");
	                    if (tab.length > 1) {
	                        listeDocument.add(tab);
	                    }
	                }
	                nbLigne++;
	            }
	        } catch (IOException e) {
	           System.out.println(e.getMessage());
	        } finally {
	            try {
	                if (lecteurAvecBuffer != null) {
	                    lecteurAvecBuffer.close();
	                }
	            } catch (Exception e) {
	                System.out.println(e.getMessage());
	            }
	        }
	        return listeDocument;
	    }



	@Override
	public void initialize(URL location, ResourceBundle resources) {
		bill.setText("8");
		tip.setText("10");
		nbPeople.setText("2");
		
		List<String[]> listing = this.readFile("Tips.txt");

		
	}
}
